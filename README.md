# AFN2AFD
###Introduction#
 Cette petite application  vient d'illustrer le fonctionnement de l'algorithme qui a pour but la determinisation d'un automate finie. ce programme est oriente web et �crit � l'aide de
* reactjs [https://facebook.github.io/react/](http://)
* react flux [https://facebook.github.io/flux/](http://)
* react-konva [https://github.com/lavrton/react-konva](http://)
* react-bootstrap [https://react-bootstrap.github.io/](http://)

il faut t�l�charger les trois branches master src et public. src et public sont des dossiers dans la branche master 
###Comment lancer le test de l'application#
Avant il faut charger les modules de **nodejs** necessaire pour le bon fonctionnement de l'application par la commande :
```
npm install

```
et puis on lance le test de l'application par:
```
npm start

```
puis on force react � utiliser konva version 1.1.6 
```
npm install react@15.X react-dom@15.X react-konva@1.1.6 konva
```
###auteurs#
BENFETTAH Oussama
BAHBABI Zakaria

